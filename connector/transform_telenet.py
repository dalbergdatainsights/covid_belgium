import pandas as pd
import glob
from tqdm import tqdm


def merge_telenet_data(path_to_data=None, file_list=None, separator=None, out=False):
    """Get a single dataframe from list of csv files of telenet format

    Keyword Arguments:
        file_list {list of str} -- List of paths to data files (csv)
        path_to_data {str} -- Root folder where the data files are located
        separator {str} -- Data separator
        columns {list of str} -- List of column names for the telenet. year, month and day have to be present!

    Returns:
        pandas.DataFrame -- [description]
    """

    assert path_to_data or file_list, "Both path_to_data and file_list cannot be empty!"

    if not separator:
        # telenet specific separator
        separator = '\t'

    # recursively look for files even if files are under their own folder
    if not file_list or len(file_list) < 1:
        file_list = set([x for x in glob.iglob(
            path_to_data + "*.csv", recursive=True)])
        print(file_list)

    print('Collecting frames')
    frames = []
    for x in tqdm(file_list):
        df = pd.read_csv(x, sep=separator, header=None)
        # this will throw an error if columns don't match
        try:
            df.columns = ['home_zip', 'trip_zip', 'telenet_trip_duration', 'total_time', 'telenet_trip_count',
                          'telenet_subscribers_in_zip', 'telenet_perc_time', 'year', 'month', 'day', 'not_relevant']
        except:
            df.columns = ['home_zip', 'trip_zip', 'telenet_trip_duration',
                          'total_time', 'telenet_perc_time', 'year', 'month', 'day', 'not_relevant']
            df['telenet_trip_count'] = -1
            df['telenet_subscribers_in_zip'] = -1
        df['year'] = df['year'].astype(str)
        df['month'] = df['month'].astype(str)
        df['day'] = df['day'].astype(str)
        df['month'] = df['month'].apply(lambda x: x if len(x) > 1 else '0'+x)
        df['day'] = df['day'].apply(lambda x: x if len(x) > 1 else '0'+x)
        df['date'] = df['year'] + df['month'] + df['day']
        df = df[['date', 'home_zip', 'trip_zip', 'telenet_subscribers_in_zip',
                 'telenet_trip_count', 'telenet_trip_duration']]
        # we don't care about the cell-based intezip mobility
        df = df[df.home_zip != df.trip_zip]
        # filter non-zip codes
        df = df[df.home_zip >= 1000]
        frames.append(df.copy())

    print('Merging...')
    return_df = pd.concat(frames)

    if out:
        return_df.to_csv('./data/input/telenet_all_days.csv')

    return return_df
