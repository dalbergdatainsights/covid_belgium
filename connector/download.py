import boto3
import os
from tqdm import tqdm


def pull_from_s3(s3_key, s3_secret, s3_bucket_name, s3_folder_name, output_folder, inplace=False):
    """Download telenet data from s3 bucket
    Keyword arguments:
    key, secret, bucket_name, folder_name -- s3 relevant parameters
    inplace -- download all files of True, download missing files on False. Default - False
    Return: return result
    """

    print('Downloading data from s3')

    def get_filename(fn):
        """Get only filenames from bucket"""
        if ('SUCCESS') in fn:
            return None
        return fn.split('/')[1]

    try:
        session = boto3.Session(aws_access_key_id=s3_key,
                                aws_secret_access_key=s3_secret)

        s3 = session.resource('s3')
        bucket = s3.Bucket(s3_bucket_name)
        files = bucket.objects.filter(Prefix=s3_folder_name)

        downloaded_files = []
        for file in tqdm(files):
            fn = get_filename(file.key)
            if fn is not None:
                fn = output_folder + fn
                if not os.path.exists(fn) or inplace:
                    bucket.download_file(Key=file.key, Filename=fn)
                    downloaded_files.append(fn)
    except Exception as e:
        return downloaded_files, e
    return downloaded_files, 'OK'
