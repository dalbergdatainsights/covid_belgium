

# Changelog

## Naming

- various folders renamed to lowercase
- according to the pep8, the __init__.py file should remain empty
- some methods are reshaped and refactored