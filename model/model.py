# Import relevant libraries
import pandas as pd
from datetime import datetime

municipality_file = './data/geo/zip_municipality_province_match.csv'
out_folder = './data/output/'


def get_export_import_df(df, operator, municipality_file=municipality_file, out=False):
    """Get dataframe with import and export per zip code for a target operator

    Arguments:
        df {pandas.DataFrame} -- Origin-Destination Data Frame with trip counts and duration (optional for given {operator})
        operator {str} -- name of the operator, lowercase (e.g. telenet)

    Keyword Arguments:
        municipality_file {str} -- path to the municipality file that matches zip to municipality to provice with codes and names (default: {municipality_file})
        out {bool} -- Should the data frame be written on the disc? Default path is ./data/temp/mobility_index.csv (default: {False})

    Returns:
        pandas.DataFrame -- DF with Import, Export and nested Province, Municipality and Postalcodes with names
    """

    df[f'{operator}_trip_count'] = df[f'{operator}_trip_count'].apply(
        lambda x: 0 if x < 0 else x)
    try:
        df[f'{operator}_trip_duration'] = df[f'{operator}_trip_duration'].apply(
            lambda x: 0 if x < 0 else x)
    except Exception as e:
        df[f'{operator}_trip_duration'] = 0
        print(e)

    # Export from region (trips out) for date
    exp = df.groupby(['date', 'home_zip']) \
        .agg({f'{operator}_subscribers_in_zip': 'max',
              f'{operator}_trip_count': 'sum',
              f'{operator}_trip_duration': 'sum'})\
        .reset_index()\
        .rename(columns={'home_zip': 'postalcode'})

    # exp = exp.set_index(['date', 'postalcode'])

    # Import to region (trips in) for date
    imp = df.groupby(['date', 'trip_zip'])\
            .agg({f'{operator}_trip_count': 'sum',
                  f'{operator}_trip_duration': 'sum'}) \
            .reset_index()\
            .rename(columns={'trip_zip': 'postalcode'})

    # imp = imp.set_index(['date', 'postalcode'])

    # if we merge on the postalcode, it creates import/export columns for specific postalcodes
    index = pd.merge(imp, exp, on=['date', 'postalcode'],
                     how='outer', suffixes=('_in', '_out'))

    geo_match = pd.read_csv(municipality_file, sep=';')

    index = pd.merge(index, geo_match, left_on='postalcode',
                     right_on='postalcode', how='left')

    # index = index.reset_index()
    index = index.fillna(0)

    # Keep only rows with no 0 values in trips or duration
    index = index[(index[f'{operator}_trip_count_in'] > 0) | (
        index[f'{operator}_trip_count_out'] > 0)]

    if out:
        # Save file with mobility/trips per date + per zip
        index.to_csv(f"./data/temp/mobility_index.csv", index=False)

    return index


def get_per_capitas(index, operator):
    """Get per capita values to the mutable df object"""
    index['in_per_capita'] = index[f'{operator}_trip_count_in'] / \
        index[f'{operator}_subscribers_in_zip']
    index['out_per_capita'] = index[f'{operator}_trip_count_out'] / \
        index[f'{operator}_subscribers_in_zip']
    index['in_out_per_capita'] = index['in_per_capita'] + \
        index['out_per_capita']
    index = index[~pd.isna(index['in_out_per_capita'])]
    return index


def get_per_capita_for_level(df, level, out=False, file_name=None):
    pass


def run(operator, df=None, out=True):

    zip_filename = f'{out_folder}{operator}_zip_per_capita.csv'
    province_filename = f'{out_folder}{operator}_province_per_capita.csv'
    national_filename = f'{out_folder}{operator}_national_per_capita.csv'

    if df is None or df.empty:
        df = pd.read_csv(f'./data/input/{operator}_all_days.csv')

    index = get_export_import_df(df, operator, out=True)
    del df
    index_zip = index[['date', 'postalcode', 'name', f'{operator}_trip_count_in',
                       f'{operator}_trip_count_out', f'{operator}_subscribers_in_zip']]

    index_zip = get_per_capitas(index_zip, operator)

    index_province = index.groupby(by=['date', 'province_nis']) \
        .agg({'province_name': 'first',
              f'{operator}_trip_count_in': 'sum',
              f'{operator}_trip_count_out': 'sum',
              f'{operator}_subscribers_in_zip': 'sum'})

    index_province = index_province.reset_index()

    index_province = get_per_capitas(index_province.reset_index(), operator)

    index_province = index_province[['date', 'province_nis', 'province_name', 'in_per_capita',
                                     'out_per_capita', 'in_out_per_capita']]

    index_national = index[[
        'date', f'{operator}_trip_count_in', f'{operator}_trip_count_out', f'{operator}_subscribers_in_zip']]

    index_national = index_national.groupby(by='date')\
        .agg({f'{operator}_trip_count_in': 'sum',
              f'{operator}_trip_count_out': 'sum',
              f'{operator}_subscribers_in_zip': 'sum'})

    index_national = index_national.reset_index()

    index_national['code'] = 32
    index_national['name'] = 'Belgium'

    index_national = get_per_capitas(index_national, operator)

    index_national = index_national[[
        'date', 'in_per_capita', 'out_per_capita', 'in_out_per_capita']]

    if out:
        index_zip.to_csv(zip_filename)
        index_national.to_csv(national_filename)
        index_province.to_csv(province_filename)

    return index_zip, index_national, index_province
